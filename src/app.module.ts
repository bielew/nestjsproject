import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersModule } from './users/users.module';
import { UsersController } from "./users/users.controller";
import { ConfigModule } from '@nestjs/config';
import * as Joi from '@hapi/joi';
import { DatabaseModule } from "./database/database.module";
import { AuthenticationModule } from './authentication/authentication.module';
import { DoughsModule } from './doughs/doughs.module';
import {APP_GUARD} from "@nestjs/core";
import {RolesGuard} from "./users/roles.guard";

@Module({
  imports: [
      UsersModule,
    ConfigModule.forRoot({
    validationSchema: Joi.object({
      POSTGRES_HOST: Joi.string().required(),
      POSTGRES_PORT: Joi.number().required(),
      POSTGRES_USER: Joi.string().required(),
      POSTGRES_PASSWORD: Joi.string().required(),
      POSTGRES_DB: Joi.string().required(),
      PORT: Joi.number(),
      JWT_SECRET: Joi.string().required(),
      JWT_EXPIRATION_TIME: Joi.string().required(),
    })
  }),
      DatabaseModule,
      AuthenticationModule,
      DoughsModule
  ],
  controllers: [AppController],
  providers: [
      AppService,
      {
      provide: APP_GUARD,
      useClass: RolesGuard,
      }
  ],
})
export class AppModule {}
