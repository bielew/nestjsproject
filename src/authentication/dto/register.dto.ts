import {Role} from "../../users/role.enum";

export class RegisterDto {
    email: string;
    name: string;
    password: string;
    role: Role;
}

export default RegisterDto;