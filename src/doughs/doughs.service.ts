import { Injectable } from '@nestjs/common';
import { CreateDoughDto } from './dto/create-dough.dto';
import { UpdateDoughDto } from './dto/update-dough.dto';

@Injectable()
export class DoughsService {
  create(createDoughDto: CreateDoughDto) {
    return 'This action adds a new dough';
  }

  findAll() {
    return `This action returns all doughs`;
  }

  findOne(id: number) {
    return `This action returns a #${id} dough`;
  }

  update(id: number, updateDoughDto: UpdateDoughDto) {
    return `This action updates a #${id} dough`;
  }

  remove(id: number) {
    return `This action removes a #${id} dough`;
  }
}
