import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { DoughsService } from './doughs.service';
import { CreateDoughDto } from './dto/create-dough.dto';
import { UpdateDoughDto } from './dto/update-dough.dto';
import { Role } from "../users/role.enum";
import { Roles } from "../users/roles.decorator";

@Controller('doughs')
export class DoughsController {
  constructor(private readonly doughsService: DoughsService) {}

  @Post()
  @Roles(Role.Admin)
  create(@Body() createDoughDto: CreateDoughDto) {
    return this.doughsService.create(createDoughDto);
  }

  @Get()
  findAll() {
    return this.doughsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.doughsService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateDoughDto: UpdateDoughDto) {
    return this.doughsService.update(+id, updateDoughDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.doughsService.remove(+id);
  }
}
