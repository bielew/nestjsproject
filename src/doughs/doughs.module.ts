import { Module } from '@nestjs/common';
import { DoughsService } from './doughs.service';
import { DoughsController } from './doughs.controller';

@Module({
  controllers: [DoughsController],
  providers: [DoughsService]
})
export class DoughsModule {}
