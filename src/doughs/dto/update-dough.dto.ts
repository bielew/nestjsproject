import { PartialType } from '@nestjs/mapped-types';
import { CreateDoughDto } from './create-dough.dto';

export class UpdateDoughDto extends PartialType(CreateDoughDto) {}
