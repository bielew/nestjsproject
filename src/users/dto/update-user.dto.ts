import { PartialType } from '@nestjs/mapped-types';
import { CreateUserDto } from './create-user.dto';
import {Role} from "../role.enum";

export class UpdateUserDto extends PartialType(CreateUserDto) {
    id: number;
    email: string;
    name: string;
    password: string;
    role: Role;
}
