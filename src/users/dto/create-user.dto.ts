import {Role} from "../role.enum";

export class CreateUserDto {
    email: string;
    name: string;
    password: string;
    role: Role;
}
