import { Module } from '@nestjs/common';
import {UsersService} from './users.service';
import { UsersController } from './users.controller';
import { User } from './entities/user.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import {RolesGuard} from "./roles.guard";
import {APP_GUARD} from "@nestjs/core";

@Module({
  imports: [TypeOrmModule.forFeature([User])],
  controllers: [UsersController],
  providers: [UsersService,
    {
      provide: APP_GUARD,
      useClass: RolesGuard,
    }],
  exports: [UsersService]
})
export class UsersModule {}
